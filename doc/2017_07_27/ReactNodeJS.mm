<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1501142369152" ID="ID_165258606" MODIFIED="1501142407188" TEXT="React + Node.JS">
<font NAME="SansSerif" SIZE="20"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1501142384115" ID="ID_767578562" MODIFIED="1501142407188" POSITION="right" TEXT="React">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1501142425785" ID="ID_1673337613" MODIFIED="1501142429040" TEXT="Core topics">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1501142429865" ID="ID_1356228357" MODIFIED="1501142437575" TEXT="Component">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1501142432133" ID="ID_965183615" MODIFIED="1501142434831" TEXT="State">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1501142916679" ID="ID_684536142" MODIFIED="1501142928740" TEXT="ES6 coding style allows to do it almost like in Java"/>
</node>
<node COLOR="#990000" CREATED="1501142753669" ID="ID_1146565339" MODIFIED="1501142755411" TEXT="Validation">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1501142831687" ID="ID_1112593025" MODIFIED="1501142845555" TEXT="Can also check that a certain property is a function"/>
</node>
<node COLOR="#990000" CREATED="1501142871504" ID="ID_918354339" MODIFIED="1501142875420" TEXT="ES6 classes">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1501142876268" ID="ID_1364809671" MODIFIED="1501142889915" TEXT="Allows to declare components the right way (like in normal languages)"/>
<node COLOR="#111111" CREATED="1501142991090" ID="ID_674060943" MODIFIED="1501143002660" TEXT="More details on ES6 style of React coding">
<node COLOR="#111111" CREATED="1501143003339" ID="ID_929693657" MODIFIED="1501143003743" TEXT="https://toddmotto.com/react-create-class-versus-component/"/>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1501142470003" ID="ID_1025421322" MODIFIED="1501142472383" TEXT="Useful links">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1501142473345" ID="ID_508298238" MODIFIED="1501142474104" TEXT="https://scotch.io/tutorials/reactjs-components-learning-the-basics">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1501142408650" ID="ID_498056933" MODIFIED="1501142410474" POSITION="left" TEXT="Node.JS">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
</node>
</node>
</map>
